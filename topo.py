"""Topography module. Helps to read topography related data."""

import json
import urllib.request as request
from pathlib import Path

dataset_url = "https://coastwatch.pfeg.noaa.gov/erddap/griddap/usgsCeSrtm30v6.json"


def generate_dataset_query_string(min_lat, max_lat, lat_step: int, min_lng, max_lng, lng_step: int) -> str:
    """Generate query string. Also place params in correct order."""
    if max_lat < min_lat:
        min_lat, max_lat = max_lat, min_lat
    if max_lng < min_lng:
        min_lng, max_lng = max_lng, min_lng
    return f"topo[({max_lat:.11g}):{lat_step:d}:({min_lat:.11g})][({min_lng:.11g}):{lng_step:d}:({max_lng:.11g})]"


def get_topo_data_from_string(data_string: str):
    """
    Return list of topography data (latitude, longitude, altitude in meters) from input string.

    The input string can be from either web or file.
    :param data_string: input string (json format)
    :return: list of lists
    """
    topo_data = json.loads(data_string)
    return topo_data['table']['rows']


def read_web(url) -> str:
    """
    Read url from the web and return contents.

    Use urllib.request to read the contents of the web page.

    :param url: URL to be read
    :return: contents of the web page (string)
    """
    try:
        with request.urlopen(url) as response:
            charset = response.info().get_param('charset')
            return response.read().decode(charset)
    except IOError:
        print(f"IOError: {url}")
    return ""


def read_json_from_web(min_lat, max_lat, lat_step: int, min_lng, max_lng, lng_step: int) -> str:
    """
    Read topography data from web and return json string.

    See the web page:
    http://coastwatch.pfeg.noaa.gov/erddap/griddap/usgsCeSrtm30v6.html

    You should use the parameters to construct url, then use
    read_web function (which you should also implement) to get the contents of the web page.

    Note that the given web page should not be used to get the contents.
    Instead, you can play with this web site to get different data and
    see how downloading data works.
    If you insert some numbers into the latitude and longitude fields,
    make sure "topo" option is checked, then choose file type: json
    and click "Just generate the URL".
    The generated URL is what you should use in here.
    See how different values on the form change the URL,
    then use parameters of this function to generate the required URL.

    Note that there are a lot of data there. If you choose large dimensions,
    the amount of data can be several giga bytes. It's wise to start with small areas.
    For example, Tallinn coordinates are:
    59.438274, 24.754352


    :param min_lat: minimum latitude
    :param max_lat: maximum latitude
    :param lat_step: step for latitude (see stride in the web page)
    :param min_lng: minimum longitude
    :param max_lng: maximum longitude
    :param lng_step: step for longitude (see stride)
    :return: json string with the results
    """
    query_string = generate_dataset_query_string(min_lat, max_lat, lat_step, min_lng, max_lng, lng_step)
    url = f"{dataset_url}?{query_string}"
    return read_web(url)


def read_json_from_file(filename: str):
    """
    Read file to get topography data. Reading file by its name has nothing to do with JSON.

    The function should just return the contents of the file.
    :param filename: filename to be opened
    :return: json string (the contents). None if the file cannot be read.
    """
    try:
        with open(filename) as file:
            return file.read()
    except IOError:
        print(f"IOError: {filename}")
    return None


def get_topography_data(min_lat, max_lat, lat_step: int, min_lng, max_lng, lng_step: int):
    """Read topography data from file or web."""
    query_string = generate_dataset_query_string(min_lat, max_lat, lat_step, min_lng, max_lng, lng_step)
    json_filename = f".\\{query_string.replace(':', '_')}.json"

    if Path(json_filename).is_file():
        content_string = read_json_from_file(json_filename)
    else:
        url = f"{dataset_url}?{query_string}"
        content_string = read_web(url)
        with open(json_filename, 'w') as file:
            file.write(content_string)

    return get_topo_data_from_string(content_string)
