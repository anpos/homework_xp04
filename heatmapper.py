"""Heatmapper module."""

import topo
from PIL import Image
from collections import namedtuple
from sys import float_info
from math import cos, pi


Point = namedtuple('Point', 'x y')
Color = namedtuple('Color', 'r g b')


class ColorPoint:
    """ColorPoint struct."""

    __slots__ = ['weight', 'color']

    def __init__(self, weight: float, color: Color):
        """Strict types."""
        self.weight, self.color = weight, color


def linear_interpolate_color(v: float, vmin: float, vmax: float, color_list: list) -> Color:
    """Linearly calculate color."""
    # index_position = value_position / value_range * max_index
    index_position = (v - vmin) / (vmax - vmin) * (len(color_list) - 1)
    index, fraction = int(index_position) // 1, index_position % 1
    # while zero can have exact representation, any calculated value should be compared with epsilon - a value
    # that makes a difference
    if fraction < float_info.epsilon:
        return color_list[index]
    c1, c2 = color_list[index], color_list[index + 1]
    return Color(int(c1.r + fraction * (c2.r - c1.r)),
                 int(c1.g + fraction * (c2.g - c1.g)),
                 int(c1.b + fraction * (c2.b - c1.b)))


def cosine_interpolate_color(v: float, vmin: float, vmax: float, color_list: list) -> Color:
    """Add some curve in calculating color."""
    index_position = (v - vmin) / (vmax - vmin) * (len(color_list) - 1)
    index, fraction = int(index_position) // 1, index_position % 1
    #
    fraction = (1 - cos(fraction * pi)) / 2
    if fraction < float_info.epsilon:
        return color_list[index]
    c1, c2 = color_list[index], color_list[index + 1]
    return Color(int(c1.r + fraction * (c2.r - c1.r)),
                 int(c1.g + fraction * (c2.g - c1.g)),
                 int(c1.b + fraction * (c2.b - c1.b)))


def cosine_path_interpolate_color(v: float, color_path: list) -> Color:
    """Linearly calculate color with defined color path."""
    pre = 0
    for cur in range(len(color_path)):
        if v >= color_path[cur].weight:
            pre = cur
        else:
            return cosine_interpolate_color(v, color_path[pre].weight, color_path[cur].weight, [color_path[pre].color, color_path[cur].color])
    return color_path[pre].color


def cosine_map_interpolate_color(vmap: float, vpath: float, color_map: list) -> Color:
    """Bi-Linearly calculate color with defined color map."""
    pre = 0
    for cur in range(len(color_map)):
        if vmap >= color_map[cur][0]:
            pre = cur
        else:
            color1 = cosine_path_interpolate_color(vpath, color_map[pre][1])  # lower altitude color path
            color2 = cosine_path_interpolate_color(vpath, color_map[cur][1])  # upper altitude color path
            color = cosine_interpolate_color(vmap, color_map[pre][0], color_map[cur][0], [color1, color2])  # latitude
            return color
    return cosine_path_interpolate_color(vpath, color_map[pre][1])


water_colors = [Color(0, 0, 127), Color(0, 0, 255), Color(0, 127, 255)]

north_pole = [
    ColorPoint(0, Color(35, 50, 20)),
    ColorPoint(50, Color(50, 100, 20)),
    ColorPoint(500, Color(100, 100, 150)),
    ColorPoint(2500, Color(150, 150, 200)),
    ColorPoint(4900, Color(255, 255, 255))
]

north_circle = [
    ColorPoint(0, Color(35, 50, 20)),
    ColorPoint(50, Color(50, 100, 20)),
    ColorPoint(500, Color(100, 150, 50)),
    ColorPoint(1000, Color(145, 150, 70)),
    ColorPoint(1500, Color(190, 150, 80)),
    ColorPoint(3000, Color(200, 200, 200)),
    ColorPoint(9000, Color(255, 255, 255))
]

north_tropic = [
    ColorPoint(0, Color(35, 50, 20)),
    ColorPoint(50, Color(190, 170, 140)),
    ColorPoint(500, Color(235, 215, 180)),
    ColorPoint(1500, Color(190, 150, 80)),
    ColorPoint(3000, Color(130, 90, 60)),
    ColorPoint(5700, Color(200, 200, 200)),
    ColorPoint(9000, Color(255, 255, 255))
]

equator = [
    ColorPoint(0, Color(35, 50, 20)),
    ColorPoint(50, Color(50, 100, 20)),
    ColorPoint(500, Color(80, 120, 50)),
    ColorPoint(1000, Color(145, 150, 70)),
    ColorPoint(1500, Color(190, 150, 80)),
    ColorPoint(3000, Color(130, 90, 60)),
    ColorPoint(4500, Color(200, 200, 200)),
    ColorPoint(9000, Color(255, 255, 255))
]

south_tropic = [
    ColorPoint(0, Color(35, 50, 20)),
    ColorPoint(50, Color(50, 100, 20)),
    ColorPoint(500, Color(100, 130, 50)),
    ColorPoint(1000, Color(130, 135, 90)),
    ColorPoint(1500, Color(190, 150, 80)),
    ColorPoint(3000, Color(130, 90, 60)),
    ColorPoint(5700, Color(200, 200, 200)),
    ColorPoint(9000, Color(255, 255, 255))
]

south_circle = [
    ColorPoint(0, Color(35, 50, 20)),
    ColorPoint(50, Color(50, 100, 20)),
    ColorPoint(500, Color(100, 150, 50)),
    ColorPoint(1000, Color(145, 150, 70)),
    ColorPoint(1500, Color(190, 150, 80)),
    ColorPoint(3000, Color(200, 200, 200)),
    ColorPoint(9000, Color(255, 255, 255))
]

south_pole = [
    ColorPoint(0, Color(35, 50, 20)),
    ColorPoint(50, Color(100, 100, 150)),
    ColorPoint(4900, Color(255, 255, 255))
]

color_path_map = [(-90.0, south_pole), (-45.0, south_circle), (-30.0, south_tropic), (0.0, equator),
                  (30.0, north_tropic), (45.0, north_circle), (90.0, north_pole)]


def generate_map(topo_data: list, width: int, height: int, filename: str) -> bool:
    """
    Generate (heat)map into an image file.

    topo_data comes from topo module. The data is a list
    where every element contains latitude, longitude and altitude (in meters).
    The function should treat coordinates as regular y and x (flat world).
    The image should fill the whole width, height. Every "point" in the data
    should be represented as a rectangle on the image.

    For example, if topo_data has 12 elements (latitude, longitude, altitude):
    10, 10, 1
    10, 12, 1
    10, 14, 2
    12, 10, 1
    12, 12, 3
    12, 14, 1
    14, 10, 6
    14, 12, 9
    14, 14, 12
    16, 10, 1
    16, 12, 1
    16, 14, 3
    and the width = 100, height = 100
    then the first line in data should be represented as a rectangle (0, 0) - (33, 25)
    (x1, y1) - (x2, y2).
    The height is divided into 4, each "point" is 100/4 = 25 pixels high,
    the width is divided into 3, each "point" is 100/3 = 33 pixels wide.
    :param topo_data: list of topography data (from topo module)
    :param width: width of the image
    :param height: height of the image
    :param filename: the file to be written
    :return: True if everything ok, False otherwise
    """
    if len(topo_data) == 0:
        return False

    altitude_min, altitude_max = 15000, -15000
    width_samples = 0
    for row in topo_data:
        if row[0] == topo_data[0][0]:  # Count width samples in data
            width_samples += 1
        if not row[2]:  # I managed to find 'null' altitude in dataset
            row[2] = 0
        if row[2] < altitude_min:
            altitude_min = row[2]
        elif row[2] > altitude_max:
            altitude_max = row[2]
    height_samples = len(topo_data) // width_samples  # As len(topo_data) == width_samples * height_samples
    width_ratio, height_ratio = width_samples / width, height_samples / height  # topo to image ratios

    img = Image.new('RGB', (width, height))
    pixels = img.load()
    for y in range(height):
        for x in range(width):
            # Convert image pixel location to appropriate topo data location
            sample = Point(int(width_ratio * x), int(height_ratio * y))
            topo_row = topo_data[sample.y * width_samples + sample.x]
            if topo_row[2] < 0:
                pixels[x, y] = linear_interpolate_color(topo_row[2], altitude_min, -1.0, water_colors)
            else:
                pixels[x, y] = cosine_map_interpolate_color(topo_row[0], topo_row[2], color_path_map)
    img.save(filename)
    img.close()
    return True


def calculate_stride_and_samples(arc_min: float, arc_max: float, samples_expected: int, stride: int = 0) -> tuple:
    """Calculate stride (if 0) or samples (if stride not 0)."""
    arc_range = float(abs(arc_max - arc_min))
    # Dataset: 1 sample per 30 arc seconds == 120 samples per 1 arc degree
    # Both min and max values are included, thus +1 sample
    samples_total = int(arc_range * 120) + 1
    # Calculate stride
    if stride == 0:
        if samples_total > samples_expected > 0:
            stride = samples_total // samples_expected
        else:
            stride = 1
    # Calculate samples
    samples = samples_total // stride
    if stride > 1:
        # Included '1' divided by stride greater than 1, would result in 0, thus +1 again
        samples += 1
    # print(f"Samples: {samples}/{samples_total} (Stride: {stride})")
    return stride, samples


def generate_map_with_coordinates(params: tuple, image_width: int, image_height: int, image_filename: str,
                                  keep_image_size: bool = True):
    """
    Given the topo parameters and image parameters, generate map into a file.

    topo_parameters = (min_latitude, max_latitude, latitude_stride, min_longitude, max_longitude, longitude_stride)
    In the case where latitude_stride and/or longitude_stride are 0,
    you have to calculate step yourself, based on the image parameters.
    For example, if image size is 10 x 10, there is no point to query more than 10 x 10 topological points.
    Hint: check the website, there you see "size" for both latitude and longitude.
    Also, read about "stride" (the question mark behind stride in the form).

    Caching:
    if all the topo params are calculated (or given), then it would be wise
    to cache the query results. One implementation could be as follows:
    filename = topo_57-60-3_22-28-1.json
    (where 57 = min_lat, 60 = max_lat, 3 latitude stride etc)
     if file exists:
         topo.read_json_from_file(file)
     else:
         result = topo.read_json_from_web(...)
         with open(filename, 'w'):
             f.write(result)

     ... do the rest


    :param params: tuple with parameters for topo query
    :param image_width: image width in pixels
    :param image_height: image height in pixels
    :param image_filename: filename to store the image
    :param keep_image_size: if true, keep given width and height
    :return: True, if everything ok, False otherwise
    """
    if keep_image_size and (image_width == 0 or image_height == 0):
        return False

    lng_step, width_samples = calculate_stride_and_samples(params[3], params[4], image_width, params[5])
    lat_step, height_samples = calculate_stride_and_samples(params[0], params[1], image_height, params[2])

    if not keep_image_size:
        # Recalculate image size. Precedence: data ratio > image ratio. Prefer lesser image size.
        if lat_step < lng_step:
            lat_step, height_samples = calculate_stride_and_samples(params[0], params[1], image_height, lng_step)
        elif lng_step < lat_step:
            lng_step, width_samples = calculate_stride_and_samples(params[3], params[4], image_width, lat_step)
        image_width, image_height = width_samples, height_samples

    data = topo.get_topography_data(params[0], params[1], lat_step, params[3], params[4], lng_step)
    return generate_map(data, image_width, image_height, image_filename)


if __name__ == '__main__':
    # topo_data = topo.get_topo_data_from_string(topo.read_json_from_web(58, 59, 10, 24, 25, 10))
    # generate_map(topo_data, 100, 100, "mymap.png")
    # generate_map_with_coordinates((57.5, 60, 0, 22, 29, 0), 1500, 0, "eesti-test.png", False)
    # generate_map_with_coordinates((57.5, 60, 0, 22, 29, 0), 1500, 1000, "eesti-resized.png", False)
    # generate_map_with_coordinates((-89.9, 90, 0, -180, 179.9, 0), 600, 400, "world.png")
    # generate_map_with_coordinates((-60, 10, 0, -90, -30, 0), 3840, 2160, "sam4K.png", False)
    generate_map_with_coordinates((-89.99166666666667, 90, 0, -180, 179.99166666666667, 0), 3840, 2160, "world4K-resized.png", False)
